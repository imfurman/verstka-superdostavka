$(document).ready(function(){
	
	// nav
	$('.navigation__toggle').on('click', function () {
		$('.navigation').toggleClass('nav_is-open');
		$('body').toggleClass('hidden');
	});
	
	// modal-mobile
	$('.modal-mobile__close').on('click', function () {
		$(this).parent('.modal-mobile').hide();
	});
	
	// slider
    $('.slider').slick({
        arrows: false,
        dots: true,
        adaptiveHeight: true
    });

    function fixBonus() {
        var docHeight =  $(document).height();
        var footerHeight =  $('.footer').height();
        var winHeight =  $(window).height();
        var scrollFromTop =  docHeight-footerHeight-winHeight;
        var $line = $('.bonus-line-wrapper');
        if ($(window).scrollTop() > scrollFromTop)
            $line.css({
                'position': 'absolute',
                'bottom': footerHeight
            });
        else
            $line.css({
                'position': 'fixed',
                'bottom': 0
            });
    }
    $(window).scroll(fixBonus);
    fixBonus();
	
	function fixLine() {
		if($(window).width() < 1041) {
			var scrollFromTop =  $('.navigation').offset().top;
			var $line = $('.navigation__wrapper');
			if ($(window).scrollTop() < scrollFromTop){
				$line.css({
					'position': 'static'
				});
			}
			else{
				$line.css({
					'position': 'fixed'
				});
			}
        }
	}
	$(window).scroll(fixLine);
	fixLine();

});
