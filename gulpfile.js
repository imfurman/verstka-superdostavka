'use strict';

var gulp = require('gulp'),
	watch = require('gulp-watch'),
	prefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	cssmin = require('gulp-cssmin'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	// pngquant = require('imagemin-pngquant'),
	rename = require('gulp-rename'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    jsmin = require('gulp-jsmin'),
	notify = require('gulp-notify');


var path = {
	build: {
		js: 'js/',
		css: 'css/'
	},
	src: {
		js: 'js/main.js',
		style: 'style/*.sass'
	},
	watch: {
		js: 'js/main.js',
		style: 'style/*.sass'
	}
};


var config = {
	server: {
		baseDir: ""
	},
	tunnel: true,
	host: 'localhost',
	port: 9000
};

gulp.task('webserver', function () {
	browserSync(config);
});


gulp.task('js:build', function () {
	gulp.src(path.src.js)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(sourcemaps.write())
        .pipe(jsmin())
		.pipe(rename({ suffix: '.min' }))
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
        .pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
		.pipe(sass())
		.pipe(prefixer())
        .pipe(rename({ suffix: '.min' }))
        .pipe(cssmin())
        .pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}))
	;
});

gulp.task('build', [
  'js:build',
  'style:build'
]);


gulp.task('watch', function(){
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
});

gulp.task('push', ['clean'], function () {
  gulp.start('build');
});

gulp.task('default', ['build', 'watch']);